package common;

import com.restproject.entities.Greeting;
import com.restproject.hello.GreetingController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Servlok on 2014-04-09.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= { GreetingController.class })
@WebAppConfiguration(value = "src/")
public class GreetingControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private GreetingController greetingController;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(greetingController).build();
    }

    @Test
    public void testGettingName() throws Exception {
        Greeting greeting = new Greeting(2,"Hello, Work!");
        GreetingController mock = org.mockito.Mockito.mock(GreetingController.class);
        when(mock.greeting("Work")).thenReturn( greeting );

        this.mockMvc.perform(get("/greeting/Work"))
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json"))
        .andExpect(jsonPath("content").value("HelloJ, Work!"));
        verifyNoMoreInteractions(mock);
    }
}
