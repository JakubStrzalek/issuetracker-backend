package unitTests;

import org.springframework.http.MediaType;

import java.nio.charset.Charset;

/**
 * Created by pblic_000 on 2014-06-11.
 */
public class TestUtil {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );
}