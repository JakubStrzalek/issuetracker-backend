package unitTests;


import com.restproject.controller.dtos.comment.CommentByIssueDTO;
import com.restproject.controller.dtos.issue.IssueDetailDTO;
import com.restproject.controller.dtos.issue.IssueGeneralDTO;
import com.restproject.controller.dtos.user.UserDTO;
import com.restproject.entities.KomentarzEntity;
import com.restproject.entities.OsobaEntity;
import com.restproject.entities.ZgloszenieEntity;

import java.sql.Timestamp;
/**
 * Created by pblic_000 on 2014-06-11.
 */
public class Initialization {

    public ZgloszenieEntity initZgloszenieEntity(int idZgloszenie,
                                          String opis,
                                          String nazwa,
                                          Timestamp data,
                                          int priorytet,
                                          int status,
                                          Timestamp dataZmianyStanu,
                                          int idOsobyZglaszajacej,
                                          int idOsobyWykonujacej,
                                          OsobaEntity osobaZglaszajaca,
                                          OsobaEntity osobaWykonujaca){
        java.util.Date date= new java.util.Date();

        ZgloszenieEntity wynik = new ZgloszenieEntity();
        wynik.setIdZgloszenie(idZgloszenie);
        wynik.setOpis(opis);
        wynik.setNazwa(nazwa);
        wynik.setData(new Timestamp(date.getTime()));
        wynik.setPriorytet(priorytet);
        wynik.setStatus(status);
        wynik.setDataZmianyStanu(new Timestamp(date.getTime()));
        wynik.setIdOsobyZglaszajacej(idOsobyZglaszajacej);
        wynik.setIdOsobyWykonujacej(idOsobyWykonujacej);
        wynik.setOsobaZglaszajaca(osobaZglaszajaca);
        wynik.setOsobaWykonujaca(osobaWykonujaca);


        return wynik;
    }

    public IssueGeneralDTO initIssueGeneralDTO(ZgloszenieEntity zgloszenieEntity){
        IssueGeneralDTO wynik = new IssueGeneralDTO();
        wynik.setIdZgloszenie(zgloszenieEntity.getIdZgloszenie());
        wynik.setNazwa(zgloszenieEntity.getNazwa());
        wynik.setPriorytet(zgloszenieEntity.getPriorytet());
        wynik.setStatus(zgloszenieEntity.getStatus());
        wynik.setData(zgloszenieEntity.getData());
        wynik.setDataZmianyStanu(zgloszenieEntity.getDataZmianyStanu());
        wynik.setOsobaZglaszajaca(zgloszenieEntity.getOsobaZglaszajaca().getImie()+" "+zgloszenieEntity.getOsobaZglaszajaca().getNazwisko());
        wynik.setOsobaWykonujaca(zgloszenieEntity.getOsobaWykonujaca().getImie()+" "+zgloszenieEntity.getOsobaWykonujaca().getNazwisko());



        return wynik;
    }


    public OsobaEntity initOsobaEntity(
            int idOsoba,
            String imie,
            String nazwisko,
            String mail,
            String haslo,
            boolean aktywny
    )
    {
        OsobaEntity wynik = new OsobaEntity();

        wynik.setIdOsoba(idOsoba);
        wynik.setImie(imie);
        wynik.setNazwisko(nazwisko);
        wynik.setMail(mail);
        wynik.setHaslo(haslo);
        wynik.setAktywny(aktywny);
        wynik.setTag(imie + " "+ nazwisko);


          return wynik;
    }

    public UserDTO initUserDTO(OsobaEntity osobaEntity){
        UserDTO wynik = new UserDTO();

        wynik.setIdOsoba(osobaEntity.getIdOsoba());
        wynik.setImie(osobaEntity.getImie());
        wynik.setNazwisko(osobaEntity.getNazwisko());
        wynik.setMail(osobaEntity.getMail());
        wynik.setAktywny(osobaEntity.isAktywny());


        return wynik;
    }

    public IssueDetailDTO initIssueDetailDTO(ZgloszenieEntity zgloszenieEntity){
        IssueDetailDTO wynik = new IssueDetailDTO();

        wynik.setIdZgloszenie(zgloszenieEntity.getIdZgloszenie());
        wynik.setOpis(zgloszenieEntity.getOpis());
        wynik.setNazwa(zgloszenieEntity.getNazwa());
        wynik.setData(zgloszenieEntity.getData());
        wynik.setPriorytet(zgloszenieEntity.getPriorytet());
        wynik.setStatus(zgloszenieEntity.getStatus());
        wynik.setDataZmianyStanu(zgloszenieEntity.getDataZmianyStanu());
        wynik.setIdOsobyZglaszajacej(zgloszenieEntity.getIdOsobyZglaszajacej());
        wynik.setIdOsobyWykonujacej(zgloszenieEntity.getIdOsobyWykonujacej());
        wynik.setOsobaZglaszajaca(initUserDTO(zgloszenieEntity.getOsobaZglaszajaca()));
        wynik.setOsobaWykonujaca(initUserDTO(zgloszenieEntity.getOsobaWykonujaca()));


        return wynik;
    }

    public KomentarzEntity initKomentarzEntity(
            int idKomentarz,
            int idOsoba,
            int idZgloszenie,
            String tresc,
            Timestamp data,
            OsobaEntity osobaKomentujaca
    ){
        KomentarzEntity wynik = new KomentarzEntity();

        wynik.setIdKomentarz(idKomentarz);
        wynik.setIdOsoba(idOsoba);
        wynik.setIdZgloszenie(idZgloszenie);
        wynik.setTresc(tresc);
        wynik.setData(data);
        wynik.setOsobaKomentujaca(osobaKomentujaca);

        return wynik;
    }

    public CommentByIssueDTO initCommentByIssueDTO(KomentarzEntity komentarzEntity)
    {
        CommentByIssueDTO wynik = new CommentByIssueDTO();

        wynik.setIdKomentarz(komentarzEntity.getIdKomentarz());
        wynik.setIdOsoba(komentarzEntity.getIdOsoba());
        wynik.setOsobaKomentujaca(komentarzEntity.getOsobaKomentujaca().getImie() + " "+komentarzEntity.getOsobaKomentujaca().getNazwisko());
        wynik.setTresc(komentarzEntity.getTresc());
        wynik.setData(komentarzEntity.getData());



        return wynik;
    }


}
