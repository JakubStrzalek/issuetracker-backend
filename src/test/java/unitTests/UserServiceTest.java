package unitTests;

import com.restproject.controller.UserController;
import com.restproject.controller.dtos.user.UserDTO;
import com.restproject.entities.OsobaEntity;
import com.restproject.services.UserService;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by pblic_000 on 2014-06-12.
 */

@RunWith(SpringJUnit4ClassRunner.class)
//@RunWith(MockitoJUnitRunner.class)
//@ContextConfiguration(classes= { IssueController.class })
@ContextConfiguration(locations={"classpath:WEB-INF/dispatcher-servlet.xml"} )
@WebAppConfiguration(value = "src/")

public class UserServiceTest {
    @Mock
    private Mapper mapper;

    @Mock
    private UserService userService;

    @InjectMocks
    UserController userController;

    private MockMvc mockMvc;

    private Initialization initialization;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        initialization = new Initialization();
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }


    @Test
    public void testGetAll() throws Exception{
        OsobaEntity osobaEntity1 = initialization.initOsobaEntity(
                1,
                "Pawel",
                "Blicharski",
                "pblicharski@wp.pl",
                "tajne",
                true
        );

        OsobaEntity osobaEntity2 = initialization.initOsobaEntity(
                2,
                "Jedrzej",
                "Modzelewski",
                "qudlatyto@gmail.com",
                "628",
                true
        );

        OsobaEntity osobaEntity3 = initialization.initOsobaEntity(
                3,
                "Hubert",
                "Strzalek",
                "s0mbrer0@gmail.com",
                "sombrerro",
                false
        );

        UserDTO userDTO1 = initialization.initUserDTO(osobaEntity1);
        UserDTO userDTO2 = initialization.initUserDTO(osobaEntity2);
        UserDTO userDTO3 = initialization.initUserDTO(osobaEntity3);

        when(userService.getAll()).thenReturn(Arrays.asList(osobaEntity1,osobaEntity2,osobaEntity3));
        when(mapper.map(osobaEntity1, UserDTO.class)).thenReturn(userDTO1);
        when( mapper.map(osobaEntity2, UserDTO.class)).thenReturn(userDTO2);
        when( mapper.map(osobaEntity3, UserDTO.class)).thenReturn(userDTO3);

        mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$", hasSize(3)))

                .andExpect(jsonPath("$[0].idOsoba", is(1)))
                .andExpect(jsonPath("$[0].imie", is("Pawel")))
                .andExpect(jsonPath("$[0].nazwisko", is("Blicharski")))
                .andExpect(jsonPath("$[0].mail", is("pblicharski@wp.pl")))
                .andExpect(jsonPath("$[0].aktywny", is(true)))

                .andExpect(jsonPath("$[1].idOsoba", is(2)))
                .andExpect(jsonPath("$[1].imie", is("Jedrzej")))
                .andExpect(jsonPath("$[1].nazwisko", is("Modzelewski")))
                .andExpect(jsonPath("$[1].mail", is("qudlatyto@gmail.com")))
                .andExpect(jsonPath("$[1].aktywny", is(true)))

                .andExpect(jsonPath("$[2].idOsoba", is(3)))
                .andExpect(jsonPath("$[2].imie", is("Hubert")))
                .andExpect(jsonPath("$[2].nazwisko", is("Strzalek")))
                .andExpect(jsonPath("$[2].mail", is("s0mbrer0@gmail.com")))
                .andExpect(jsonPath("$[2].aktywny", is(false)));

    }

    @Test
    public void testGet() throws Exception {
        OsobaEntity osobaEntity1 = initialization.initOsobaEntity(
                1,
                "Pawel",
                "Blicharski",
                "pblicharski@wp.pl",
                "tajne",
                true
        );

        UserDTO userDTO1 = initialization.initUserDTO(osobaEntity1);
        when(userService.get(1)).thenReturn(osobaEntity1);
        when(mapper.map(osobaEntity1, UserDTO.class)).thenReturn(userDTO1);


        mockMvc.perform(get("/user/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.idOsoba", is(1)))
                .andExpect(jsonPath("$.imie", is("Pawel")))
                .andExpect(jsonPath("$.nazwisko", is("Blicharski")))
                .andExpect(jsonPath("$.mail", is("pblicharski@wp.pl")))
                .andExpect(jsonPath("$.aktywny", is(true)));
    }




}
