package unitTests;

import com.restproject.entities.OsobaEntity;
import com.restproject.entities.ZgloszenieEntity;
import com.restproject.services.business.IssuePriorityValidator;
import com.restproject.services.business.IssueStatusManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Timestamp;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by pblic_000 on 2014-06-12.
 */


@RunWith(SpringJUnit4ClassRunner.class)
//@RunWith(MockitoJUnitRunner.class)
//@ContextConfiguration(classes= { IssueController.class })
@ContextConfiguration(locations={"classpath:WEB-INF/dispatcher-servlet.xml"} )
@WebAppConfiguration(value = "src/")
public class BuisnessTest {

    IssuePriorityValidator issuePriorityValidator;
    IssueStatusManager issueStatusManager;
    ZgloszenieEntity first;
    Initialization initialization;
    OsobaEntity osobaEntity;


    @Before
    public void setup() {
    issuePriorityValidator = new IssuePriorityValidator();
    issueStatusManager = new IssueStatusManager();
    initialization = new Initialization();

    osobaEntity = initialization.initOsobaEntity(
                1,
                "Pawel",
                "Blicharski",
                "pblicharski@wp.pl",
                "tajne",
                true
    );


    first = initialization.initZgloszenieEntity(
            1,
            "Zrobcie to szybko!",
            "Zgloszenie1",
            new Timestamp(1),
            1,
            1,
            new Timestamp(1),
            1,
            1,
            osobaEntity,
            osobaEntity
    );
    }


    @Test
    public void IssuePriorityValidatorTest() throws Exception {

        assertTrue(issuePriorityValidator.isValid(1));
        assertTrue(issuePriorityValidator.isValid(2));
        assertTrue(issuePriorityValidator.isValid(3));

        assertTrue(!issuePriorityValidator.isValid(0));
        assertTrue(!issuePriorityValidator.isValid(4));
        assertTrue(!issuePriorityValidator.isValid(10));
        assertTrue(!issuePriorityValidator.isValid(102));
        assertTrue(!issuePriorityValidator.isValid(11));
        assertTrue(!issuePriorityValidator.isValid(4));

    }

    @Test
    public void IssueStatusManagerTest() throws Exception{

        assertTrue(true);

    }

}
