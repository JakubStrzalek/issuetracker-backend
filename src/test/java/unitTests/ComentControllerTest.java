package unitTests;

import com.restproject.controller.CommentController;
import com.restproject.controller.dtos.comment.CommentByIssueDTO;
import com.restproject.entities.KomentarzEntity;
import com.restproject.entities.OsobaEntity;
import com.restproject.services.CommentService;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Timestamp;
import java.util.Arrays;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by pblic_000 on 2014-06-12.
 */

@RunWith(SpringJUnit4ClassRunner.class)
//@RunWith(MockitoJUnitRunner.class)
//@ContextConfiguration(classes= { IssueController.class })
@ContextConfiguration(locations={"classpath:WEB-INF/dispatcher-servlet.xml"} )
@WebAppConfiguration(value = "src/")
public class ComentControllerTest {


    @Mock
    private CommentService commentService;

    @Mock
    private Mapper mapper;

    @InjectMocks
    private CommentController commentController;

    private MockMvc mockMvc;

    private Initialization initialization;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        initialization = new Initialization();
        mockMvc = MockMvcBuilders.standaloneSetup(commentController).build();
    }


    @Test
    public void getCommentsByIssueId() throws Exception{

        OsobaEntity osobaEntity = initialization.initOsobaEntity(
                1,
                "Pawel",
                "Blicharski",
                "pblicharski@wp.pl",
                "tajne",
                true
        );

        KomentarzEntity first= initialization.initKomentarzEntity(
                1,
                1,
                1,
                "szybciej!",
                new Timestamp(1),
                osobaEntity
        );


        KomentarzEntity second= initialization.initKomentarzEntity(
                1,
                1,
                1,
                "szybciej!",
                new Timestamp(1),
                osobaEntity
        );


        KomentarzEntity third= initialization.initKomentarzEntity(
                1,
                1,
                1,
                "szybciej!",
                new Timestamp(1),
                osobaEntity
        );



        CommentByIssueDTO firstDTO = initialization.initCommentByIssueDTO(first);
        CommentByIssueDTO secondDTO = initialization.initCommentByIssueDTO(second);
        CommentByIssueDTO thirdDTO = initialization.initCommentByIssueDTO(third);

        when(commentService.getByIssue(1)).thenReturn(Arrays.asList(first, second, third));
        when( mapper.map(first, CommentByIssueDTO.class)).thenReturn(firstDTO);
        when( mapper.map(second,CommentByIssueDTO.class)).thenReturn(secondDTO);
        when( mapper.map(third,CommentByIssueDTO.class)).thenReturn(thirdDTO);

        mockMvc.perform(get("/comment?idZgloszenia=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$",hasSize(3)))

                .andExpect(jsonPath("$[0].idKomentarz", is(1)))
                .andExpect(jsonPath("$[0].idOsoba", is(1)))
                .andExpect(jsonPath("$[0].osobaKomentujaca", is("Pawel Blicharski")))
                .andExpect(jsonPath("$[0].tresc", is("szybciej!")))

                .andExpect(jsonPath("$[1].idKomentarz", is(1)))
                .andExpect(jsonPath("$[1].idOsoba", is(1)))
                .andExpect(jsonPath("$[1].osobaKomentujaca", is("Pawel Blicharski")))
                .andExpect(jsonPath("$[1].tresc", is("szybciej!")))

                .andExpect(jsonPath("$[2].idKomentarz", is(1)))
                .andExpect(jsonPath("$[2].idOsoba", is(1)))
                .andExpect(jsonPath("$[2].osobaKomentujaca", is("Pawel Blicharski")))
                .andExpect(jsonPath("$[2].tresc", is("szybciej!")));


    }
}
