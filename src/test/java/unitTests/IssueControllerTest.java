package unitTests;

import com.restproject.controller.IssueController;
import com.restproject.controller.dtos.issue.IssueDetailDTO;
import com.restproject.controller.dtos.issue.IssueGeneralDTO;
import com.restproject.entities.OsobaEntity;
import com.restproject.entities.ZgloszenieEntity;
import com.restproject.services.IssueService;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Timestamp;
import java.util.Arrays;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by pblic_000 on 2014-06-11.
 */
/*
* import com.restproject.entities.Greeting;
import com.restproject.hello.GreetingController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
*/
@RunWith(SpringJUnit4ClassRunner.class)
//@RunWith(MockitoJUnitRunner.class)
//@ContextConfiguration(classes= { IssueController.class })
@ContextConfiguration(locations={"classpath:WEB-INF/dispatcher-servlet.xml"} )
@WebAppConfiguration(value = "src/")

public class IssueControllerTest {



    @Mock
    private IssueService issueService;

    @Mock
    private Mapper mapper;

    @InjectMocks
    private IssueController issueController;

    private MockMvc mockMvc;

    private Initialization initialization;



    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        initialization = new Initialization();
        mockMvc = MockMvcBuilders.standaloneSetup(issueController).build();
    }

    @Test
    public void testGetAll() throws  Exception{

        OsobaEntity osobaEntity = initialization.initOsobaEntity(
                1,
                "Pawel",
                "Blicharski",
                "pblicharski@wp.pl",
                "tajne",
                true
        );

        ZgloszenieEntity first= initialization.initZgloszenieEntity(
                1,
                "Zrobcie to szybko!",
                "Zgloszenie1",
                new Timestamp(1),
                1,
                1,
                new Timestamp(1),
                1,
                1,
                osobaEntity,
                osobaEntity
                );

        ZgloszenieEntity second= initialization.initZgloszenieEntity(
                1,
                "Zrobcie to wolno!",
                "Zgloszenie2",
                new Timestamp(1),
                1,
                1,
                new Timestamp(1),
                1,
                1,
                osobaEntity,
                osobaEntity
                );
        IssueGeneralDTO firstDTO = initialization.initIssueGeneralDTO(first);
        IssueGeneralDTO secondDTO = initialization.initIssueGeneralDTO(second);

//        mapper = org.mockito.Mockito.mock(DozerBeanMapper.class);
//        issueService = org.mockito.Mockito.mock(RestIssueService.class);
  //      LinkedList<ZgloszenieEntity> lista = new LinkedList<ZgloszenieEntity>();
    //    lista.add(first);
        when(issueService.getAll()).thenReturn(Arrays.asList(first, second));
        when( mapper.map(first, IssueGeneralDTO.class)).thenReturn(firstDTO);
        when( mapper.map(second,IssueGeneralDTO.class)).thenReturn(secondDTO);


        mockMvc.perform(get("/issue"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].idZgloszenie", is(1)))
                .andExpect(jsonPath("$[0].nazwa", is("Zgloszenie1")))
                .andExpect(jsonPath("$[0].priorytet", is(1)))
                .andExpect(jsonPath("$[0].status", is(1)))
                .andExpect(jsonPath("$[0].osobaZglaszajaca", is("Pawel Blicharski")))
                .andExpect(jsonPath("$[0].osobaWykonujaca", is("Pawel Blicharski")))
                .andExpect(jsonPath("$[1].idZgloszenie", is(1)))
                .andExpect(jsonPath("$[1].nazwa", is("Zgloszenie2")))
                .andExpect(jsonPath("$[1].priorytet", is(1)))
                .andExpect(jsonPath("$[1].status", is(1)))
                .andExpect(jsonPath("$[1].osobaZglaszajaca", is("Pawel Blicharski")))
                .andExpect(jsonPath("$[1].osobaWykonujaca", is("Pawel Blicharski")));
    }


    @Test
    public void testGetUserById() throws  Exception {
        OsobaEntity osobaEntity = initialization.initOsobaEntity(
                1,
                "Jedrek",
                "Modzelewski",
                "qudlatyto@wp.pl",
                "628",
                true
        );

        ZgloszenieEntity first= initialization.initZgloszenieEntity(
                1,
                "Jedrzeju ma to byc na wczoraj!",
                "Blad z polaczeniem",
                new Timestamp(1),
                1,
                1,
                new Timestamp(1),
                1,
                1,
                osobaEntity,
                osobaEntity
        );

        ZgloszenieEntity second= initialization.initZgloszenieEntity(
                1,
                "Zrobcie to szybko!",
                "Kernal Panic",
                new Timestamp(1),
                1,
                1,
                new Timestamp(1),
                1,
                1,
                osobaEntity,
                osobaEntity
        );
        IssueGeneralDTO firstDTO = initialization.initIssueGeneralDTO(first);
        IssueGeneralDTO secondDTO = initialization.initIssueGeneralDTO(second);

//        mapper = org.mockito.Mockito.mock(DozerBeanMapper.class);
//        issueService = org.mockito.Mockito.mock(RestIssueService.class);
        //      LinkedList<ZgloszenieEntity> lista = new LinkedList<ZgloszenieEntity>();
        //    lista.add(first);
        when(issueService.getByUser(1)).thenReturn(Arrays.asList(first, second));
        when( mapper.map(first, IssueGeneralDTO.class)).thenReturn(firstDTO);
        when( mapper.map(second,IssueGeneralDTO.class)).thenReturn(secondDTO);


        mockMvc.perform(get("/issue?idOsoby=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[0].idZgloszenie", is(1)))
                .andExpect(jsonPath("$[0].nazwa", is("Blad z polaczeniem")))
                .andExpect(jsonPath("$[0].priorytet", is(1)))
                .andExpect(jsonPath("$[0].status", is(1)))
                .andExpect(jsonPath("$[0].osobaZglaszajaca", is("Jedrek Modzelewski")))
                .andExpect(jsonPath("$[0].osobaWykonujaca", is("Jedrek Modzelewski")))
                .andExpect(jsonPath("$[1].idZgloszenie", is(1)))
                .andExpect(jsonPath("$[1].nazwa", is("Kernal Panic")))
                .andExpect(jsonPath("$[1].priorytet", is(1)))
                .andExpect(jsonPath("$[1].status", is(1)))
                .andExpect(jsonPath("$[1].osobaZglaszajaca", is("Jedrek Modzelewski")))
                .andExpect(jsonPath("$[1].osobaWykonujaca", is("Jedrek Modzelewski")));
    }


    @Test
    public void testGetIssueById() throws  Exception {

        OsobaEntity osobaEntity = initialization.initOsobaEntity(
                1,
                "Jedrek",
                "Modzelewski",
                "qudlatyto@gmail.com",
                "628",
                true
        );


        ZgloszenieEntity first= initialization.initZgloszenieEntity(
                1,
                "Jedrzeju ma to byc na wczoraj!",
                "Blad z polaczeniem",
                new Timestamp(1),
                1,
                1,
                new Timestamp(1),
                1,
                1,
                osobaEntity,
                osobaEntity
        );

        IssueDetailDTO firstDTO  =  initialization.initIssueDetailDTO(first);

      //  when(issueService.get(1)).thenReturn(first);
        when( mapper.map(issueService.get(1), IssueDetailDTO.class)).thenReturn(firstDTO);

        mockMvc.perform(get("/issue/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.nazwa",is("Blad z polaczeniem") ))
                .andExpect(jsonPath("$.opis", is("Jedrzeju ma to byc na wczoraj!")))
                .andExpect(jsonPath("$.nazwa", is("Blad z polaczeniem")))
                .andExpect(jsonPath("$.priorytet", is(1)))
                .andExpect(jsonPath("$.status", is(1)))
                .andExpect(jsonPath("$.idOsobyWykonujacej", is(1)))
                .andExpect(jsonPath("$.idOsobyZglaszajacej", is(1)))
                .andExpect(jsonPath("$.osobaZglaszajaca.idOsoba", is(1)))
                .andExpect(jsonPath("$.osobaZglaszajaca.imie", is("Jedrek")))
                .andExpect(jsonPath("$.osobaWykonujaca.aktywny", is(true)))
                .andExpect(jsonPath("$.osobaWykonujaca.mail", is("qudlatyto@gmail.com")));

    }



}
