package com.restproject.services;

import com.restproject.entities.ZgloszenieEntity;

import java.util.List;

/**
 * Created by Servlok on 2014-06-11.
 */
public interface IssueService {
    List<ZgloszenieEntity> getAll();

    List<ZgloszenieEntity> getByUser(int id);

    ZgloszenieEntity get(int id);

    ZgloszenieEntity create(ZgloszenieEntity entity);

    void updateAssignedUser(int idAssignedUser, int id);

    void update(ZgloszenieEntity entity, int id);
}
