package com.restproject.services;

import com.restproject.dao.IssueDAO;
import com.restproject.entities.ZgloszenieEntity;
import com.restproject.services.business.IssueStatusManager;
import com.restproject.services.business.PriorityValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Servlok on 2014-06-11.
 */
@Service
@Transactional
public class RestIssueService implements IssueService {

    @Autowired
    private PriorityValidator priorityValidator;

    @Autowired
    private MailService mailService;

    @Autowired
    private IssueDAO issueDAO;

    @Override
    public List<ZgloszenieEntity> getAll() {
        return issueDAO.getAll();
    }

    @Override
    public List<ZgloszenieEntity> getByUser(int id) {
        return issueDAO.getIssueByUser(id);
    }

    @Override
    public ZgloszenieEntity get(int id) {
        ZgloszenieEntity issue = issueDAO.getById(id);
        if(issue == null){
            throw new IllegalArgumentException();
        }
        return issue;
    }

    @Override
    public ZgloszenieEntity create(ZgloszenieEntity entity) {
        if(!priorityValidator.isValid(entity.getPriorytet())) {
            throw new IllegalArgumentException();
        }
        java.util.Date date = new java.util.Date();
        entity.setData(new Timestamp(date.getTime()));
        entity.setDataZmianyStanu(new Timestamp(date.getTime()));
        entity.setStatus(IssueStatusManager.OPENED);
        issueDAO.create( entity );
        return issueDAO.getById(entity.getIdZgloszenie());
    }

    @Override
    public void updateAssignedUser(int idAssignedUser, int id) {
        ZgloszenieEntity entity = issueDAO.getById(id);
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        entity.setIdOsobyWykonujacej(idAssignedUser);
        issueDAO.update(entity);

        if(entity.getOsobaWykonujaca().isAktywny()) {
            mailService.sendAssignedUserNotificationMail(entity.getOsobaWykonujaca().getMail(), entity.getNazwa());
        }
    }

    @Override
    public void update(ZgloszenieEntity issue, int id) {
        if(!priorityValidator.isValid(issue.getPriorytet())) {
            throw new IllegalArgumentException();
        }

        ZgloszenieEntity entity = issueDAO.getById(id);
        if(entity == null) {
            throw new IllegalArgumentException();
        }

        entity.setOpis(issue.getOpis());
        entity.setPriorytet(issue.getPriorytet());
        entity.setNazwa(issue.getNazwa());
        issueDAO.update(entity);
    }

}
