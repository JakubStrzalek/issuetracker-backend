package com.restproject.services.business;

import com.restproject.entities.ZgloszenieEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Servlok on 2014-06-08.
 */
@Service
public class IssueStatusManager implements StatusManager {

    public final static int CREATED = 1;
    public final static int OPENED = 2;
    public final static int IN_PROGRESS = 3;
    public final static int HOLD = 4;
    public final static int VERIFIED = 5;
    public final static int CLOSED = 6;
    public final static int CANCELED = 7;

    final static int LEGAL_STATES_TO_CREATED[] = { };
    final static int LEGAL_STATES_TO_OPENED[] = { CREATED, IN_PROGRESS, VERIFIED };
    final static int LEGAL_STATES_TO_IN_PROGRESS[] = { OPENED, HOLD };
    final static int LEGAL_STATES_TO_HOLD[] = { IN_PROGRESS };
    final static int LEGAL_STATES_TO_VERIFIED[] = { IN_PROGRESS };
    final static int LEGAL_STATES_TO_CLOSED[] = { VERIFIED };
    final static int LEGAL_STATES_TO_CANCELED[] = { CREATED, OPENED, IN_PROGRESS, HOLD, VERIFIED, CLOSED };

    private boolean checkStatusCriteria(int state, int[] acceptedStates ) {
        for(int acceptedState : acceptedStates) {
            if(state == acceptedState)
                return true;
        }

        return false;
    }

    private void defaultStatusChange(ZgloszenieEntity issue,int status) {
        issue.setStatus(status);
        Date date = new Date();
        issue.setDataZmianyStanu(new Timestamp(date.getTime()));
    }


    @Override
    public void cancel(ZgloszenieEntity issue) {
        if(!checkStatusCriteria(issue.getStatus(),LEGAL_STATES_TO_CANCELED)) {
            throw new IllegalArgumentException();
        }
        defaultStatusChange(issue, CANCELED);
    }

    @Override
    public void open(ZgloszenieEntity issue) {

        if(!checkStatusCriteria(issue.getStatus(),LEGAL_STATES_TO_OPENED)) {
            throw new IllegalArgumentException();
        }

        defaultStatusChange(issue,OPENED);
    }

    @Override
    public void progress(ZgloszenieEntity issue) {
        if(!checkStatusCriteria(issue.getStatus(),LEGAL_STATES_TO_IN_PROGRESS)) {
            throw new IllegalArgumentException();
        }
        defaultStatusChange(issue,IN_PROGRESS);
    }

    @Override
    public void hold(ZgloszenieEntity issue) {
        if(!checkStatusCriteria(issue.getStatus(),LEGAL_STATES_TO_HOLD)) {
            throw new IllegalArgumentException();
        }
        defaultStatusChange(issue,HOLD);
    }

    @Override
    public void verify(ZgloszenieEntity issue) {
        if(!checkStatusCriteria(issue.getStatus(),LEGAL_STATES_TO_VERIFIED)) {
            throw new IllegalArgumentException();
        }
        defaultStatusChange(issue,VERIFIED);
    }

    @Override
    public void close(ZgloszenieEntity issue) {
        if(!checkStatusCriteria(issue.getStatus(),LEGAL_STATES_TO_CLOSED)) {
            throw new IllegalArgumentException();
        }
        defaultStatusChange(issue,CLOSED);
    }
}
