package com.restproject.services.business;

import com.restproject.entities.ZgloszenieEntity;

/**
 * Created by Servlok on 2014-06-08.
 */
public interface StatusManager {

    void cancel(ZgloszenieEntity issue);

    void open(ZgloszenieEntity issue);

    void progress(ZgloszenieEntity issue);

    void hold(ZgloszenieEntity issue);

    void verify(ZgloszenieEntity issue);

    void close(ZgloszenieEntity issue);
}
