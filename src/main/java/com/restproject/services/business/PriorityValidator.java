package com.restproject.services.business;

/**
 * Created by Servlok on 2014-06-11.
 */

public interface PriorityValidator {
    public boolean isValid(int priority);
}
