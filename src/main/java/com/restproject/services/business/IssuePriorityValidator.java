package com.restproject.services.business;

import org.springframework.stereotype.Service;

/**
 * Created by Servlok on 2014-06-11.
 */
@Service
public class IssuePriorityValidator implements PriorityValidator {
    @Override
    public boolean isValid(int priority) {
        if(priority < 1 || priority > 3) {
            return false;
        }
        return true;
    }
}
