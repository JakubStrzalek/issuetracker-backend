package com.restproject.services.business;

/**
 * Created by Servlok on 2014-06-12.
 */
public class StatusOperation {
    private int idZgloszenie;
    private int operationType;

    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenie) {
        this.idZgloszenie = idZgloszenie;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }
}
