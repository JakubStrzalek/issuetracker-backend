package com.restproject.services;

import com.restproject.entities.OsobaEntity;

import java.util.List;

/**
 * Created by Servlok on 2014-06-11.
 */
public interface UserService {
    List<OsobaEntity> getAll();
    OsobaEntity get(int id);
}
