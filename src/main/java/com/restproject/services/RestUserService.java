package com.restproject.services;

import com.restproject.dao.UserJpaDAO;
import com.restproject.entities.OsobaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Servlok on 2014-06-11.
 */
@Service
@Transactional
public class RestUserService implements UserService {

    @Autowired
    private UserJpaDAO userDAO;

    @Override
    public List<OsobaEntity> getAll() {
        return userDAO.getAll();
    }

    @Override
    public OsobaEntity get(int id) {
        OsobaEntity user = userDAO.getById(id);
        if(user == null) {
            throw new IllegalArgumentException();
        }
        return user;
    }
}
