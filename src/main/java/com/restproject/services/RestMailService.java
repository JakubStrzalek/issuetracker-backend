package com.restproject.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * Created by Servlok on 2014-06-12.
 */
@Service
public class RestMailService implements MailService {

    @Autowired
    private MailSender mailSender;

    final static String NOTIFICATION_MAIL_SUBJECT = "Przypisano do Ciebie zgloszenie o nazwie: ";

    final static String NOTIFICATION_MAIL_BODY = "Odwiedz aplikacje IssueTracker aby dowiedziec sie wiecej!";

    final static String NEW_COMMENT_MAIL_SUBJECT = "Skomentowano zgloszenie do ktorego jestes przypisany o nazwie: ";


    private void sendMail(String to, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        mailSender.send(message);
    }

    @Override
    public void sendAssignedUserNotificationMail(String to, String issueName) {
        sendMail(to,NOTIFICATION_MAIL_SUBJECT + issueName, NOTIFICATION_MAIL_BODY);
    }

    @Override
    public void sendNewCommentNotificationMail(String to, String issueName, String commentingUser, String text) {
        sendMail(to,NEW_COMMENT_MAIL_SUBJECT + issueName,
                "Uzytkownik: "+ commentingUser + " " +
                "Dodal komentarz o tresci: " + text + " " +
                NOTIFICATION_MAIL_BODY );
    }

}
