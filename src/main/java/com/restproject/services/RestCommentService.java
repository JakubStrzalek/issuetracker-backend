package com.restproject.services;

import com.restproject.dao.CommentDAO;
import com.restproject.dao.IssueDAO;
import com.restproject.dao.UserDAO;
import com.restproject.entities.KomentarzEntity;
import com.restproject.entities.OsobaEntity;
import com.restproject.entities.ZgloszenieEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Servlok on 2014-06-11.
 */
@Service
@Transactional
public class RestCommentService implements CommentService {
    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private IssueDAO issueDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private MailService mailService;

    @Override
    public List<KomentarzEntity> getByIssue(int id) {
        return commentDAO.getCommentsByIssueId(id);
    }

    @Override
    public KomentarzEntity create(KomentarzEntity comment) {
        java.util.Date date = new java.util.Date();
        comment.setData(new Timestamp(date.getTime()));
        commentDAO.create(comment);

        ZgloszenieEntity issue = issueDAO.getById(comment.getIdZgloszenie());
        OsobaEntity user = issue.getOsobaWykonujaca();
        OsobaEntity userComment = userDAO.getById(comment.getIdOsoba());

        if(user.isAktywny()) {
            mailService.sendNewCommentNotificationMail(user.getMail(),issue.getNazwa(),userComment.getTag(),comment.getTresc());
        }

        return comment;
    }

    @Override
    public void update(String text, int id) {
        KomentarzEntity entity = commentDAO.getById(id);
        if(entity == null) {
            throw new IllegalArgumentException();
        }
        entity.setTresc(text);
        commentDAO.update(entity);
    }

    @Override
    public void delete(int id) {
        commentDAO.deleteById(id);
    }
}
