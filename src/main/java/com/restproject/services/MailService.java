package com.restproject.services;

/**
 * Created by Servlok on 2014-06-12.
 */
public interface MailService {
    public void sendAssignedUserNotificationMail(String to, String issueName);

    public void sendNewCommentNotificationMail(String to, String issueName, String commentingUser, String text);

}
