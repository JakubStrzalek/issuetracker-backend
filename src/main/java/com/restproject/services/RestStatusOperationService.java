package com.restproject.services;

import com.restproject.dao.IssueDAO;
import com.restproject.entities.ZgloszenieEntity;
import com.restproject.services.business.IssueStatusManager;
import com.restproject.services.business.StatusManager;
import com.restproject.services.business.StatusOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Servlok on 2014-06-11.
 */
@Service
@Transactional
public class RestStatusOperationService implements StatusOperationService {

    @Autowired
    private IssueDAO issueDAO;

    @Autowired
    private StatusManager statusManager;

    @Override
    public void create(StatusOperation operation) {
        ZgloszenieEntity ent = issueDAO.getById(operation.getIdZgloszenie());
        if( ent == null) {
            throw new IllegalArgumentException();
        }
        switch (operation.getOperationType()) {
            case IssueStatusManager.OPENED:
                statusManager.open(ent);
                break;

            case IssueStatusManager.IN_PROGRESS:
                statusManager.progress(ent);
                break;

            case IssueStatusManager.CLOSED:
                statusManager.close(ent);
                break;

            case IssueStatusManager.HOLD:
                statusManager.hold(ent);
                break;

            case IssueStatusManager.CANCELED:
                statusManager.cancel(ent);
                break;

            case IssueStatusManager.VERIFIED:
                statusManager.verify(ent);
                break;


            default: throw new IllegalArgumentException();
        }

        issueDAO.update(ent);
    }
}
