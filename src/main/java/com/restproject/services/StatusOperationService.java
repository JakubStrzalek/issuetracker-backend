package com.restproject.services;

import com.restproject.entities.ZgloszenieEntity;
import com.restproject.services.business.StatusOperation;

/**
 * Created by Servlok on 2014-06-11.
 */
public interface StatusOperationService {
    void create(StatusOperation entity);
}
