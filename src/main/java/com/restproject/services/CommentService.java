package com.restproject.services;

import com.restproject.entities.KomentarzEntity;

import java.util.List;

/**
 * Created by Servlok on 2014-06-11.
 */
public interface CommentService {
    List<KomentarzEntity> getByIssue(int id);

    KomentarzEntity create(KomentarzEntity comment );

    void update(String text,int id);

    void delete(int id);
}
