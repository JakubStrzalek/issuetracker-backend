package com.restproject.controller;

import com.restproject.dao.UserJpaDAO;
import com.restproject.entities.OsobaEntity;
import com.restproject.controller.dtos.user.UserDTO;
import com.restproject.services.UserService;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by pblic_000 on 2014-05-29.
 */


@Controller
@Transactional
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private Mapper mapper;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<UserDTO> user() {
        List<OsobaEntity> users = userService.getAll();
        List<UserDTO> usersDTOs = new LinkedList<UserDTO>();
        for( OsobaEntity ent : users) {
            usersDTOs.add( mapper.map(ent, UserDTO.class));
        }

        return usersDTOs;
    }



    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    UserDTO getIssueById(@PathVariable("id") int id) throws IllegalArgumentException {
        OsobaEntity user = userService.get(id);
        return mapper.map(user, UserDTO.class);
    }

}
