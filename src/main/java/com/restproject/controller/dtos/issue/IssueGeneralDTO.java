package com.restproject.controller.dtos.issue;

import java.sql.Timestamp;

/**
 * Created by Servlok on 2014-06-03.
 */
public class IssueGeneralDTO {
    private int idZgloszenie;
    private String nazwa;
    private int priorytet;
    private int status;
    private Timestamp data;
    private Timestamp dataZmianyStanu;
    private String osobaZglaszajaca;
    private String osobaWykonujaca;

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    public Timestamp getDataZmianyStanu() {
        return dataZmianyStanu;
    }

    public void setDataZmianyStanu(Timestamp dataZmianyStanu) {
        this.dataZmianyStanu = dataZmianyStanu;
    }

    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenie) {
        this.idZgloszenie = idZgloszenie;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOsobaZglaszajaca() {
        return osobaZglaszajaca;
    }

    public void setOsobaZglaszajaca(String osobaZglaszajaca) {
        this.osobaZglaszajaca = osobaZglaszajaca;
    }

    public String getOsobaWykonujaca() {
        return osobaWykonujaca;
    }

    public void setOsobaWykonujaca(String osobaWykonujaca) {
        this.osobaWykonujaca = osobaWykonujaca;
    }
}
