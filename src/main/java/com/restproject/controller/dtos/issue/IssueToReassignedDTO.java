package com.restproject.controller.dtos.issue;

/**
 * Created by Servlok on 2014-06-09.
 */
public class IssueToReassignedDTO {
    private int idZgloszenie;
    private int idOsobyWykonujacej;

    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenia) {
        this.idZgloszenie = idZgloszenia;
    }

    public int getIdOsobyWykonujacej() {
        return idOsobyWykonujacej;
    }

    public void setIdOsobyWykonujacej(int idOsobyWykonujacej) {
        this.idOsobyWykonujacej = idOsobyWykonujacej;
    }
}
