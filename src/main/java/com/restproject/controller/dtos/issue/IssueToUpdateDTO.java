package com.restproject.controller.dtos.issue;

/**
 * Created by Servlok on 2014-06-10.
 */
public class IssueToUpdateDTO {
    private String opis;
    private String nazwa;
    private int priorytet;

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }
}
