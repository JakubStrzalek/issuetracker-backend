package com.restproject.controller.dtos.issue;

import java.sql.Timestamp;

/**
 * Created by Servlok on 2014-06-12.
 */
public class IssueWhichAddedDTO {
    private int idZgloszenie;
    private Timestamp data;

    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenie) {
        this.idZgloszenie = idZgloszenie;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }
}
