package com.restproject.controller.dtos.issue;


/**
 * Created by Servlok on 2014-06-04.
 */
public class IssueToAddDTO {

    private String opis;
    private String nazwa;
    private int priorytet;
    private int idOsobyZglaszajacej;
    private int idOsobyWykonujacej;


    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }

    public int getIdOsobyZglaszajacej() {
        return idOsobyZglaszajacej;
    }

    public void setIdOsobyZglaszajacej(int idOsobyZglaszajacej) {
        this.idOsobyZglaszajacej = idOsobyZglaszajacej;
    }

    public int getIdOsobyWykonujacej() {
        return idOsobyWykonujacej;
    }

    public void setIdOsobyWykonujacej(int idOsobyWykonujacej) {
        this.idOsobyWykonujacej = idOsobyWykonujacej;
    }
}
