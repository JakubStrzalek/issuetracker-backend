package com.restproject.controller.dtos.issue;

/**
 * Created by Servlok on 2014-06-08.
 */
public class IssueToChangeDTO {
    private int idZgloszenie;
    private int operationType;

    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenia) {
        this.idZgloszenie = idZgloszenia;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }
}
