package com.restproject.controller.dtos.issue;

import com.restproject.controller.dtos.user.UserDTO;

import java.sql.Timestamp;

/**
 * Created by Servlok on 2014-06-03.
 */
public class IssueDetailDTO {
    private int idZgloszenie;
    private String opis;
    private String nazwa;
    private Timestamp data;
    private int priorytet;
    private int status;
    private Timestamp dataZmianyStanu;
    private int idOsobyZglaszajacej;
    private int idOsobyWykonujacej;
    private UserDTO osobaZglaszajaca;
    private UserDTO osobaWykonujaca;

    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenie) {
        this.idZgloszenie = idZgloszenie;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    public int getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getDataZmianyStanu() {
        return dataZmianyStanu;
    }

    public void setDataZmianyStanu(Timestamp dataZmianyStanu) {
        this.dataZmianyStanu = dataZmianyStanu;
    }

    public int getIdOsobyZglaszajacej() {
        return idOsobyZglaszajacej;
    }

    public void setIdOsobyZglaszajacej(int idOsobyZglaszajacej) {
        this.idOsobyZglaszajacej = idOsobyZglaszajacej;
    }

    public int getIdOsobyWykonujacej() {
        return idOsobyWykonujacej;
    }

    public void setIdOsobyWykonujacej(int idOsobyWykonujacej) {
        this.idOsobyWykonujacej = idOsobyWykonujacej;
    }

    public UserDTO getOsobaZglaszajaca() {
        return osobaZglaszajaca;
    }

    public void setOsobaZglaszajaca(UserDTO osobaZglaszajaca) {
        this.osobaZglaszajaca = osobaZglaszajaca;
    }

    public UserDTO getOsobaWykonujaca() {
        return osobaWykonujaca;
    }

    public void setOsobaWykonujaca(UserDTO osobaWykonujaca) {
        this.osobaWykonujaca = osobaWykonujaca;
    }
}
