package com.restproject.controller.dtos.comment;

import java.sql.Timestamp;

/**
 * Created by Servlok on 2014-06-12.
 */
public class CommentWhichAddedDTO {
    private int idKomentarz;
    private Timestamp data;

    public int getIdKomentarz() {
        return idKomentarz;
    }

    public void setIdKomentarz(int idKomentarz) {
        this.idKomentarz = idKomentarz;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }
}
