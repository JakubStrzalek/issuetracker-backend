package com.restproject.controller.dtos.comment;

import java.sql.Timestamp;

/**
 * Created by Servlok on 2014-06-06.
 */
public class CommentByIssueDTO {
    private int idKomentarz;
    private int idOsoba;
    private String osobaKomentujaca;
    private String tresc;
    private Timestamp data;

    public int getIdKomentarz() {
        return idKomentarz;
    }

    public void setIdKomentarz(int idKomentarz) {
        this.idKomentarz = idKomentarz;
    }

    public String getOsobaKomentujaca() {
        return osobaKomentujaca;
    }

    public void setOsobaKomentujaca(String osobaKomentujaca) {
        this.osobaKomentujaca = osobaKomentujaca;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }


    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public int getIdOsoba() {
        return idOsoba;
    }

    public void setIdOsoba(int idOsoba) {
        this.idOsoba = idOsoba;
    }
}
