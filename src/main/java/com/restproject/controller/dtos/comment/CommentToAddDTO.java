package com.restproject.controller.dtos.comment;

/**
 * Created by Servlok on 2014-06-07.
 */
public class CommentToAddDTO {
    private int idOsoba;
    private int idZgloszenie;
    private String tresc;

    public int getIdOsoba() {
        return idOsoba;
    }

    public void setIdOsoba(int idOsoba) {
        this.idOsoba = idOsoba;
    }

    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenie) {
        this.idZgloszenie = idZgloszenie;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }
}
