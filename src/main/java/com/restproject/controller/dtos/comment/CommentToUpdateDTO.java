package com.restproject.controller.dtos.comment;

/**
 * Created by Servlok on 2014-06-11.
 */
public class CommentToUpdateDTO {
    private String tresc;

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }
}
