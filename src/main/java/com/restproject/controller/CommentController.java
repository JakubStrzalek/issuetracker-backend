package com.restproject.controller;

import com.restproject.controller.dtos.comment.CommentWhichAddedDTO;
import com.restproject.dao.CommentDAO;
import com.restproject.entities.KomentarzEntity;
import com.restproject.controller.dtos.comment.CommentByIssueDTO;
import com.restproject.controller.dtos.comment.CommentToAddDTO;
import com.restproject.controller.dtos.comment.CommentToUpdateDTO;
import com.restproject.services.CommentService;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Servlok on 2014-06-06.
 */
@Controller
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @Autowired
    private Mapper mapper;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<CommentByIssueDTO> getCommentsByIssue(@RequestParam(value="idZgloszenia", required = true) int idZgloszenia) {
       List<KomentarzEntity> entities = commentService.getByIssue(idZgloszenia);
       List<CommentByIssueDTO> commentByIssueDTOs = new LinkedList<CommentByIssueDTO>();
       for( KomentarzEntity ent : entities ) {
           commentByIssueDTOs.add( mapper.map(ent,CommentByIssueDTO.class ));
       }

       return commentByIssueDTOs;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    CommentWhichAddedDTO createIssueById(@RequestBody CommentToAddDTO commentDTO) {
        KomentarzEntity comment = mapper.map(commentDTO, KomentarzEntity.class);
        comment = commentService.create( comment );
        return mapper.map(comment, CommentWhichAddedDTO.class);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody
    void updateIssue(@RequestBody CommentToUpdateDTO commentDTO, @PathVariable("id") int id) throws IllegalArgumentException {
        commentService.update(commentDTO.getTresc(),id);

    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public @ResponseBody
    void  delete(@PathVariable("id") int id) {
        commentService.delete(id);
    }

}
