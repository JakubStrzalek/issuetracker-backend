package com.restproject.controller;

import com.restproject.dao.IssueDAO;
import com.restproject.entities.ZgloszenieEntity;
import com.restproject.services.StatusOperationService;
import com.restproject.services.business.StatusManager;
import com.restproject.services.business.IssueStatusManager;
import com.restproject.controller.dtos.issue.IssueToChangeDTO;
import com.restproject.services.business.StatusOperation;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Servlok on 2014-06-08.
 */
@Controller
@Transactional
@RequestMapping("/statusoperation")
public class StatusOperationController {

    @Autowired
    private Mapper mapper;

    @Autowired
    private StatusOperationService statusOperationService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    void createOperation(@RequestBody IssueToChangeDTO issue) throws IllegalArgumentException {
        StatusOperation operation = mapper.map(issue, StatusOperation.class);
        statusOperationService.create(operation);


    }


}
