package com.restproject.controller;

import com.restproject.dao.IssueDAO;
import com.restproject.entities.ZgloszenieEntity;
import com.restproject.services.IssueService;
import com.restproject.services.business.PriorityValidator;
import com.restproject.controller.dtos.issue.*;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Servlok on 2014-05-09.
 */
@Controller
@Transactional
@RequestMapping("/issue")
public class IssueController {

    @Autowired
    private IssueService issueService;

    @Autowired
    private Mapper mapper;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<IssueGeneralDTO> issue(@RequestParam(value="idOsoby", required = false) Integer id) {
        List<ZgloszenieEntity> entities;
        if(id == null) {
            entities =  issueService.getAll();
        } else {
            entities = issueService.getByUser(id);
        }
        List<IssueGeneralDTO> issueGeneralDTOs = new LinkedList<IssueGeneralDTO>();
        for( ZgloszenieEntity ent : entities ) {
            issueGeneralDTOs.add( mapper.map(ent,IssueGeneralDTO.class ));
        }

        return issueGeneralDTOs;
    }


    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    IssueDetailDTO getIssueById(@PathVariable("id") int id) throws IllegalArgumentException{
        ZgloszenieEntity issue = issueService.get(id);
        return mapper.map(issue,IssueDetailDTO.class);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    IssueWhichAddedDTO createIssue(@RequestBody IssueToAddDTO issue) throws IllegalArgumentException {
        ZgloszenieEntity entity = mapper.map(issue,ZgloszenieEntity.class);
        entity = issueService.create(entity);
        return mapper.map(entity, IssueWhichAddedDTO.class);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",method = RequestMethod.PATCH, produces = "application/json")
    public @ResponseBody
    void changeAssignedUser(@RequestBody IssueToReassignedDTO issue, @PathVariable("id") int id) throws IllegalArgumentException {
        issueService.updateAssignedUser(issue.getIdOsobyWykonujacej(),id);

    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value= "/{id}", method = RequestMethod.PUT, produces = "application/json")
    public  @ResponseBody
    void updateIssue(@RequestBody IssueToUpdateDTO issue, @PathVariable("id") int id) throws IllegalArgumentException {
        ZgloszenieEntity entity = mapper.map(issue, ZgloszenieEntity.class);
        issueService.update(entity,id);

    }
}
