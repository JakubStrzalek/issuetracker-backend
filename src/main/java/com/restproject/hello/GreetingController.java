package com.restproject.hello;

import com.restproject.entities.Greeting;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Servlok on 2014-04-08.
 */
@Controller
@RequestMapping("/greeting")
public class GreetingController {
    private static final String template = "HelloJ, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value="/{name}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Greeting greeting(@PathVariable String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

}
