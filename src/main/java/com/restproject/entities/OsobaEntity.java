package com.restproject.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by pblic_000 on 2014-05-27.
 */
@Entity
@Table(name = "Osoba", schema = "", catalog = "pik")
public class OsobaEntity implements Serializable {
    private int idOsoba;
    private String imie;
    private String nazwisko;
    private String mail;
    private String haslo;
    private boolean aktywny;
    private String tag;

    @PostLoad
    @PostUpdate
    @PostPersist
    void initTag() {
        tag = getImie() + " " + getNazwisko();
    }

    @Id
    @Column(name = "Id_osoba")
    public int getIdOsoba() {
        return idOsoba;
    }

    public void setIdOsoba(int idOsoba) {
        this.idOsoba = idOsoba;
    }

    @Basic
    @Column(name = "Imie")
    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    @Basic
    @Column(name = "Nazwisko")
    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    @Basic
    @Column(name = "Mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Basic
    @Column(name = "Haslo")
    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    @Basic
    @Column(name = "Aktywny")
    public boolean isAktywny() {
        return aktywny;
    }

    public void setAktywny(boolean aktywny) {
        this.aktywny = aktywny;
    }

    @Transient
    public String getTag() {
        return tag;
    }

    @Transient
    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OsobaEntity that = (OsobaEntity) o;

        if (aktywny != that.aktywny) return false;
        if (idOsoba != that.idOsoba) return false;
        if (haslo != null ? !haslo.equals(that.haslo) : that.haslo != null) return false;
        if (imie != null ? !imie.equals(that.imie) : that.imie != null) return false;
        if (mail != null ? !mail.equals(that.mail) : that.mail != null) return false;
        if (nazwisko != null ? !nazwisko.equals(that.nazwisko) : that.nazwisko != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idOsoba;
        result = 31 * result + (imie != null ? imie.hashCode() : 0);
        result = 31 * result + (nazwisko != null ? nazwisko.hashCode() : 0);
        result = 31 * result + (mail != null ? mail.hashCode() : 0);
        result = 31 * result + (haslo != null ? haslo.hashCode() : 0);
        result = 31 * result + (aktywny ? 1 : 0);
        return result;
    }
}
