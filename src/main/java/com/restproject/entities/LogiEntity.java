package com.restproject.entities;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by pblic_000 on 2014-05-27.
 */
@Entity
@Table(name = "Logi", schema = "", catalog = "pik")
public class LogiEntity {
    private int idZgloszenie;
    private String opis;
    private String nazwa;
    private Date data;
    private int priorytet;
    private int status;
    private Date dataZmianyStanu;

    @Id
    @Column(name = "Id_zgloszenie")
    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenie) {
        this.idZgloszenie = idZgloszenie;
    }

    @Basic
    @Column(name = "Opis")
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Basic
    @Column(name = "Nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Basic
    @Column(name = "Data")
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Basic
    @Column(name = "Priorytet")
    public int getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }

    @Basic
    @Column(name = "Status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "Data_zmiany_stanu")
    public Date getDataZmianyStanu() {
        return dataZmianyStanu;
    }

    public void setDataZmianyStanu(Date dataZmianyStanu) {
        this.dataZmianyStanu = dataZmianyStanu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogiEntity that = (LogiEntity) o;

        if (idZgloszenie != that.idZgloszenie) return false;
        if (priorytet != that.priorytet) return false;
        if (status != that.status) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (dataZmianyStanu != null ? !dataZmianyStanu.equals(that.dataZmianyStanu) : that.dataZmianyStanu != null)
            return false;
        if (nazwa != null ? !nazwa.equals(that.nazwa) : that.nazwa != null) return false;
        if (opis != null ? !opis.equals(that.opis) : that.opis != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idZgloszenie;
        result = 31 * result + (opis != null ? opis.hashCode() : 0);
        result = 31 * result + (nazwa != null ? nazwa.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + priorytet;
        result = 31 * result + status;
        result = 31 * result + (dataZmianyStanu != null ? dataZmianyStanu.hashCode() : 0);
        return result;
    }
}
