package com.restproject.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by pblic_000 on 2014-05-27.
 */
@NamedQueries(
        @NamedQuery(
                name="KomentarzEntity.getCommentsByIssueId",
                query="SELECT c FROM KomentarzEntity c WHERE c.idZgloszenie = :idZgloszenie"
        )
)
@Entity
@Table(name = "Komentarz", schema = "", catalog = "pik")
public class KomentarzEntity implements Serializable{
    private int idKomentarz;
    private int idOsoba;
    private int idZgloszenie;
    private String tresc;
    private Timestamp data;

    private OsobaEntity osobaKomentujaca;

    @Id
    @GeneratedValue
    @Column(name = "Id_komentarz")
    public int getIdKomentarz() {
        return idKomentarz;
    }

    public void setIdKomentarz(int idKomentarz) {
        this.idKomentarz = idKomentarz;
    }

    @Basic
    @Column(name = "Tresc")
    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    @Basic
    @Column(name = "Data")
    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    @Basic
    @Column(name = "Id_osoby")
    public int getIdOsoba() {
        return idOsoba;
    }

    public void setIdOsoba(int idOsoba) {
        this.idOsoba = idOsoba;
    }

    @Basic
    @Column(name = "Id_zgloszenia")
    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenie) {
        this.idZgloszenie = idZgloszenie;
    }

    @ManyToOne
    @JoinColumn(name = "Id_osoby", insertable = false, updatable = false)
    public OsobaEntity getOsobaKomentujaca() {
        return osobaKomentujaca;
    }

    public void setOsobaKomentujaca(OsobaEntity osobaKomentujaca) {
        this.osobaKomentujaca = osobaKomentujaca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KomentarzEntity that = (KomentarzEntity) o;

        if (idKomentarz != that.idKomentarz) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (tresc != null ? !tresc.equals(that.tresc) : that.tresc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKomentarz;
        result = 31 * result + (tresc != null ? tresc.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }
}
