package com.restproject.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by pblic_000 on 2014-05-27.
 */
@NamedQueries({
        @NamedQuery(
                name = "ZgloszenieEntity.updateAssignedUser",
                query = "UPDATE ZgloszenieEntity SET idOsobyWykonujacej = :idOsobyWykonujacej WHERE idZgloszenie = :idZgloszenie"
        ),
        @NamedQuery(
                name = "ZgloszenieEntity.getIssueByUser",
                query = "SELECT c FROM ZgloszenieEntity c WHERE c.idOsobyWykonujacej = :idOsobyWykonujacej"
        )
})
@Entity
@Table(name = "Zgloszenie", schema = "", catalog = "pik")
public class ZgloszenieEntity implements Serializable{
    private int idZgloszenie;
    private String opis;
    private String nazwa;
    private Timestamp data;
    private int priorytet;
    private int status;
    private Timestamp dataZmianyStanu;
    private int idOsobyZglaszajacej;
    private int idOsobyWykonujacej;
    private OsobaEntity osobaZglaszajaca;
    private OsobaEntity osobaWykonujaca;

    @Id
    @GeneratedValue
    @Column(name = "Id_zgloszenie")
    public int getIdZgloszenie() {
        return idZgloszenie;
    }

    public void setIdZgloszenie(int idZgloszenie) {
        this.idZgloszenie = idZgloszenie;
    }

    @Basic
    @Column(name = "Opis")
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Basic
    @Column(name = "Nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Basic
    @Column(name = "Data", insertable = false)
    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    @Basic
    @Column(name = "Priorytet")
    public int getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }

    @Basic
    @Column(name = "Status", insertable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "Data_zmiany_stanu" ,insertable = false)
    public Timestamp getDataZmianyStanu() {
        return dataZmianyStanu;
    }

    public void setDataZmianyStanu(Timestamp dataZmianyStanu) {
        this.dataZmianyStanu = dataZmianyStanu;
    }

    @Column(name = "Id_osoby_zglaszajacej")
    public int getIdOsobyZglaszajacej() {
        return idOsobyZglaszajacej;
    }

    public void setIdOsobyZglaszajacej(int idOsobyZglaszajacej) {
        this.idOsobyZglaszajacej = idOsobyZglaszajacej;
    }

    @Column(name = "Id_osoby_wykonujacej")
    public int getIdOsobyWykonujacej() {
        return idOsobyWykonujacej;
    }

    public void setIdOsobyWykonujacej(int idOsobyWykonujacej) {
        this.idOsobyWykonujacej = idOsobyWykonujacej;
    }

    @ManyToOne
    @JoinColumn(name = "Id_osoby_zglaszajacej",insertable = false, updatable = false)
    public OsobaEntity getOsobaZglaszajaca() {
        return osobaZglaszajaca;
    }


    public void setOsobaZglaszajaca(OsobaEntity osobaZglaszajaca) {
        this.osobaZglaszajaca = osobaZglaszajaca;
    }

    @ManyToOne
    @JoinColumn(name = "Id_osoby_wykonujacej",insertable = false, updatable = false)
    public OsobaEntity getOsobaWykonujaca() {
        return osobaWykonujaca;
    }


    public void setOsobaWykonujaca(OsobaEntity osobaWykonujaca) {
        this.osobaWykonujaca = osobaWykonujaca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ZgloszenieEntity that = (ZgloszenieEntity) o;

        if (idZgloszenie != that.idZgloszenie) return false;
        if (priorytet != that.priorytet) return false;
        if (status != that.status) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (dataZmianyStanu != null ? !dataZmianyStanu.equals(that.dataZmianyStanu) : that.dataZmianyStanu != null)
            return false;
        if (nazwa != null ? !nazwa.equals(that.nazwa) : that.nazwa != null) return false;
        if (opis != null ? !opis.equals(that.opis) : that.opis != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idZgloszenie;
        result = 31 * result + (opis != null ? opis.hashCode() : 0);
        result = 31 * result + (nazwa != null ? nazwa.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + priorytet;
        result = 31 * result + status;
        result = 31 * result + (dataZmianyStanu != null ? dataZmianyStanu.hashCode() : 0);
        return result;
    }
}
