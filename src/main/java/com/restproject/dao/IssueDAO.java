package com.restproject.dao;

import com.restproject.entities.ZgloszenieEntity;

import java.util.List;
/**
 * Created by Servlok on 2014-05-09.
 */
public interface IssueDAO {

    //IssueDAO
    void create(ZgloszenieEntity issue);

    List<ZgloszenieEntity> getAll();

    ZgloszenieEntity getById(int Id);

    List<ZgloszenieEntity> getIssueByUser(int id);

    public void update(ZgloszenieEntity entity);

    void updateAssignedUser(ZgloszenieEntity entity);

}
