package com.restproject.dao;

import com.restproject.entities.KomentarzEntity;

import java.util.List;

/**
 * Created by Servlok on 2014-06-06.
 */
public interface CommentDAO {

    public List<KomentarzEntity> getCommentsByIssueId(int idIssue);

    public void create( KomentarzEntity entity );

    public void deleteById(int id);

    public KomentarzEntity getById(int id);

    public void update(KomentarzEntity entity);
}
