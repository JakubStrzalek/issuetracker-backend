package com.restproject.dao;

import com.restproject.entities.OsobaEntity;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by pblic_000 on 2014-05-29.
 */
@Repository
public class UserJpaDAO extends AbstractJpaDAO<OsobaEntity> implements UserDAO {

    private static final String ALL_OSOBY =
            "SELECT s FROM OsobaEntity s";

    @Override
    public List<OsobaEntity> getAll() {
        return entityManager.createQuery(ALL_OSOBY).getResultList();
    }

    @Override
    public void create(OsobaEntity user) {
//        entityManager.getTransaction().begin();
        entityManager.persist(user);
//        entityManager.getTransaction().commit();
    }


    @Override
    public OsobaEntity getById(int Id) {
        return entityManager.find(OsobaEntity.class, Id);
    }
}
