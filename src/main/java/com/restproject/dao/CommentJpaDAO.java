package com.restproject.dao;

import com.restproject.entities.KomentarzEntity;
import com.restproject.entities.ZgloszenieEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.util.List;

/**
 * Created by Servlok on 2014-06-06.
 */

@Repository
public class CommentJpaDAO extends AbstractJpaDAO<KomentarzEntity> implements CommentDAO {


    @Override
    public List<KomentarzEntity> getCommentsByIssueId(int idIssue) {
        return entityManager.createNamedQuery("KomentarzEntity.getCommentsByIssueId", KomentarzEntity.class).setParameter("idZgloszenie",idIssue).getResultList();
    }

    @Override
    public void deleteById(int id) {
        KomentarzEntity entity = entityManager.find(KomentarzEntity.class, id);
        delete( entity );
    }

    @Override
    public KomentarzEntity getById(int id) {
        return entityManager.find(KomentarzEntity.class, id);
    }




}
