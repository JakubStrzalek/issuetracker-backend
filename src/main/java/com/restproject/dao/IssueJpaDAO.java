package com.restproject.dao;


import com.restproject.entities.ZgloszenieEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Servlok on 2014-05-09.
 */
@Repository
public class IssueJpaDAO extends AbstractJpaDAO<ZgloszenieEntity> implements IssueDAO {

    //JPAIssueDAO
    private static final String ALL_ISSUE =
            //"SELECT s,o1,o2 FROM ZgloszenieEntity s, OsobaEntity o1, OsobaEntity o2 WHERE s.idOsobyZglaszajacej = o1.id and s.idOsobyWykonujacej = o2.id";
            "SELECT s FROM ZgloszenieEntity  s";


    @Override
    public void create(ZgloszenieEntity issue) {
        entityManager.persist(issue);
    }

    @Override
    public List<ZgloszenieEntity> getAll() {
        return entityManager.createQuery(ALL_ISSUE).getResultList();
    }

    @Override
    public ZgloszenieEntity getById(int Id) {
        return entityManager.find(ZgloszenieEntity.class, Id);
    }

    @Override
    public List<ZgloszenieEntity> getIssueByUser(int id) {
        return entityManager.createNamedQuery("ZgloszenieEntity.getIssueByUser").
                setParameter("idOsobyWykonujacej", id).
                getResultList();
    }


    @Override
    public void updateAssignedUser(ZgloszenieEntity entity) {
         entityManager.createNamedQuery("ZgloszenieEntity.updateAssignedUser").
                setParameter("idZgloszenie",entity.getIdZgloszenie()).
                setParameter("idOsobyWykonujacej",entity.getIdOsobyWykonujacej()).
                getResultList();
    }

}
