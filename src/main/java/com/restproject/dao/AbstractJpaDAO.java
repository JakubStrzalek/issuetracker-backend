package com.restproject.dao;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Servlok on 2014-05-09.
 */
public abstract class AbstractJpaDAO< T extends Serializable> {
    private Class< T > clazz;

    @PersistenceContext
    EntityManager entityManager;

    public void create( T entity ){
        entityManager.persist( entity );
    }

    public void delete( T entity ){
        entityManager.remove( entity );
    }

    public void update( T entity ) { entityManager.merge( entity ); }
}
