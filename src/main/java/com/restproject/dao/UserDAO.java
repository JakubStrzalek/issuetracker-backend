package com.restproject.dao;

import com.restproject.entities.OsobaEntity;

import java.util.List;

/**
 * Created by pblic_000 on 2014-05-29.
 */


public interface UserDAO {


    void create(OsobaEntity user);

    List<OsobaEntity> getAll();

    OsobaEntity getById(int Id);

}
